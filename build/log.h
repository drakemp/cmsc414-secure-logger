#ifndef LOG_H
#define LOG_H

#include"crypt.h" //lengths

#define EXIT_BAD_ARG 255
#define EXIT 255

#define MAX_NUMBER 1073741823
//pack struct

typedef struct log_header_keys{
   unsigned char iv[IV_LENGTH];
   unsigned char iv2[IV_LENGTH]; 
   unsigned char token_hmac[KEY_LENGTH];
   unsigned char hmac[KEY_LENGTH];
} Log_Header_Keys;

typedef struct log_header_data{
   unsigned long timestamp;
   unsigned long num_of_logs;
} Log_Header_Data;


#define MAX_NAME_LENGTH 13

typedef struct __attribute__((__packed__)) name{
   char n[MAX_NAME_LENGTH+1];
   unsigned char type;  
} Name;

#define GUEST 1
#define EMPLOYEE 0

#define ARRIVAL 0
#define LEAVING 1

#define GALLERY -1 //look later
#define LEFT_GALLERY -2

typedef struct __attribute__((__packed__)) log{
   Name name;
   unsigned long room;
   unsigned char status;
   unsigned long timestamp;
} Log;

//function prototypes
void print_current_state(Log *logs, int length);
void print_room_list(Log *logs, int length, char *name, int type);
#endif

//True/False defines
#ifndef TRUE_FALSE
#define TRUE_FALSE

#define TRUE 1
#define FALSE 0

#endif
