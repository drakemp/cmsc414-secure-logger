#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include <ctype.h>

#include"crypt.h"
#include"log.h"

#define EXIT_BAD_ARG 255
#define EXIT 255

int main(int argc, char *argv[]){
   char *timestamp=NULL,
      *token=NULL,
      *guest_name=NULL,
      *employee_name=NULL,
      *room=NULL,
      *log=NULL;
   short arrival=FALSE, departure=FALSE;
   unsigned char token_hash[KEY_LENGTH];
   int log_fd;
   int opt = -1;
   int i = 0;
   char *ptr;
   long temp;

   //////////////////////////
   /* CHECK FOR VALID ARGS */
   //////////////////////////

  //pick up the switches
   while ((opt = getopt(argc, argv, "T:K:E:G:ALR:B:")) != -1) {
      switch(opt) {
      case 'B':
		 fprintf(stdout, "unimplemented\n");
		 exit(EXIT_BAD_ARG);
         break;
	 
      case 'T':
         //timestamp
		 timestamp = optarg;
		 temp = strtol(timestamp, &ptr, 10);
		 if (temp == 0) {
			 fprintf(stdout, "invalid\n");
			 exit(EXIT_BAD_ARG);
		 } else if (temp < 0 || temp > MAX_NUMBER) {
			 fprintf(stdout, "invalid\n");
			 exit(EXIT_BAD_ARG);
		 }

         break;

      case 'K':
	 //secret token
	 token = optarg;
	 for (i = 0; i < strlen(token); i++) {
		if (!isalpha(token[i]) && !isdigit(token[i])) {
			fprintf(stdout, "invalid\n");
			exit(EXIT_BAD_ARG);
		}
	 }
	 break;

      case 'A':
	 //arrival
	 if (!departure)
	    arrival=TRUE;
	 else{
		fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      case 'L':
	 //departure
	 if (!arrival)
	    departure=TRUE; 
	 else{
		fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      case 'E':
	 //employee name
	 if (!guest_name) {
	    employee_name = optarg;
	 	for (i = 0; i < strlen(employee_name); i++) {
	 	   if (!isalpha(employee_name[i])) {
			fprintf(stdout, "invalid\n");
	 	   	exit(EXIT_BAD_ARG);
	 	   }
	 	}
	 }else{
		fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      case 'G':
	 //guest name
	 if (!employee_name) {
	    guest_name = optarg;
	 	for (i = 0; i < strlen(guest_name); i++) {
	 	   if (!isalpha(guest_name[i])) {
			fprintf(stdout, "invalid\n");
	 	   	exit(EXIT_BAD_ARG);
	 	   }
		}
	 }else{
		fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      case 'R':
        //room ID
	 room = optarg;
	 short is_zero = TRUE;
	 for (i=0; i < strlen(room); i++) {
		if(room[i] != '0') {
			is_zero = FALSE; break;
		}
	 }		
 	 temp = strtol(room, &ptr, 10);
 	 if (temp == 0 && !is_zero) {
		 fprintf(stdout, "invalid\n");
 	     exit(EXIT_BAD_ARG);
 	 } else if (temp < 0 || temp > MAX_NUMBER) {
		 fprintf(stdout, "invalid\n");
 	     exit(EXIT_BAD_ARG);
 	 }
	 break;
	 
      default:
	 fprintf(stdout, "invalid\n");
	 exit(255);	    
	 break;
      }
   }
   //pick up the positional argument for log path
   if(optind < argc && optind+1 == argc) {
	   //check if logname is numbers, _, ., /, or alphabetical chars
      log = argv[optind];
	  for (i=0; i < strlen(log); i++) {
	 	   if (!isalpha(log[i]) && log[i] != '_' && log[i] != '/' 
				   && log[i] != '.' && !isdigit(log[i])) {
			fprintf(stdout, "invalid\n");
	 	   	exit(EXIT_BAD_ARG);
	 	   }
	  }

   } else {
	  fprintf(stdout, "invalid\n");
      exit(EXIT);
   }


   /* Main Logic */
   Log_Header_Keys header_keys;
   Log_Header_Data header_data;
   Log log_s;
   int data_length;
   
   /* check if out file exists */
   if(access(log, F_OK) !=  -1){
   	
   	    /////////////////
   		/* FILE EXISTS */
   		/////////////////
      
		//Check to see if token is correct
      unsigned char *encrypted_data;
      int file_size, bytes_read, log_fd, i;
	  
      //open file and get size
      log_fd = open(log, O_RDWR, 0664);
      file_size = lseek(log_fd, 0, SEEK_END);
      lseek(log_fd, 0, SEEK_SET);
      //read in header
      bytes_read = read(log_fd, &header_keys, sizeof(Log_Header_Keys));
      if (bytes_read != sizeof(Log_Header_Keys)) {
	 return EXIT;
      }

      //Read in the encrypted data
      int log_bytes = file_size-sizeof(Log_Header_Keys); //Get log size
      encrypted_data = malloc(log_bytes);
      bytes_read = read(log_fd, encrypted_data, log_bytes);
      if (bytes_read != log_bytes) {
	 return EXIT;
      }

      //set up for hash
      unsigned char *mes = malloc(strlen(token) + IV_LENGTH);
      memcpy(mes, header_keys.iv, IV_LENGTH);
      memcpy(mes+IV_LENGTH, token, strlen(token));
      hash(mes, strlen(token)+IV_LENGTH, token_hash);
	  
	  free(mes);
      //verify that header is correct
      int hash_verify = hmac_verification(token_hash, KEY_LENGTH,
			  header_keys.token_hmac, header_keys.iv);

      if (!hash_verify) {
		fprintf(stdout, "invalid\n"); 
	 	return EXIT;
      }

      int hmac_verify = hmac_verification(encrypted_data, bytes_read,
					  header_keys.hmac, token_hash);
      
	  if (!hmac_verify) {
		fprintf(stdout, "invalid\n"); 
	 	return EXIT;
      }

      //Decrypt now
     int total = 0;
      //Decrypting header, remember decrypting outputs length of previous
      //block, so this should return 0.
      if((data_length = crypt(encrypted_data, sizeof(Log_Header_Data),
			      (unsigned char *)&header_data, token_hash, 
			      header_keys.iv2, DECRYPT))){
	 return EXIT;
      }

      if (atol(timestamp) <= header_data.timestamp) {
	 fprintf(stdout, "invalid\n");
	 return EXIT;
      }

      Log *logs = malloc(sizeof(Log)*(header_data.num_of_logs+1));
      
      //Decrypting logs all at once, add a 32 byte offset for header because
      //it was being padded 
      if((data_length = crypt(encrypted_data+32, sizeof(Log)*(header_data.num_of_logs),
			      (unsigned char *)logs, token_hash,
			      header_keys.iv2, DECRYPT))){
	 return EXIT;
      }
      
      free(encrypted_data);
   		///////////////////
   		/* CONSTRUCT LOG */
   		///////////////////
      if (guest_name){
	 strncpy(log_s.name.n, guest_name, MAX_NAME_LENGTH);
	 log_s.name.n[MAX_NAME_LENGTH] = '\0';
	 log_s.name.type = GUEST;
      } else if (employee_name) {
	 strncpy(log_s.name.n, employee_name, MAX_NAME_LENGTH);
	 log_s.name.n[MAX_NAME_LENGTH] = '\0';
	 log_s.name.type = EMPLOYEE;
      } else {
	 close(log_fd);
	 return EXIT;
      }
      
      //validate that these are propper longs
      log_s.timestamp = atol(timestamp);
      log_s.status = arrival? ARRIVAL : LEAVING;
      if (departure) //Case of -L without room, should leave gallery
	 	log_s.room = room? atol(room) : LEFT_GALLERY;
      else //Case of -A without room, should enter gallery
	 	log_s.room = room? atol(room) : GALLERY;
      
	 short found = FALSE;
      for (i=header_data.num_of_logs-1; i>=0; i--) {
   		//////////////////
   		/* PROCESS LOGS */
   		//////////////////
	 
	 //Start backwards, find the last log of person of interest
	 //only need to check this data for appending
	 //An employee and guest could have same name, so must check
	 //if names are the same and if type sare same

	 if (!(strcmp(logs[i].name.n, log_s.name.n))
	     && (logs[i].name.type == log_s.name.type)) {
		found = TRUE;
	    break;
	 }
      }
      
      //Found last log so now need to ensure that it we can add a new one
      //Case of -A with -R
	 if (found) {
      if (room && arrival) {
	 if(logs[i].room == LEFT_GALLERY) {//if they left the gallery, must reenter first
		fprintf(stdout, "invalid\n");
        close(log_fd);
		return EXIT;
	 }
	 
	 if(logs[i].status != LEAVING && logs[i].room != GALLERY) {
	    //to enter a room, must leave previous room or just entered gallery
		fprintf(stdout, "invalid\n");
	    close(log_fd);
	    return EXIT;
	 }
	 
      } else if (!room && arrival) {
	 if(logs[i].status != LEAVING) {
		 fprintf(stdout, "invalid\n");
		 return EXIT;
	  }
	  }
      
      //Case of -L with -R
      if (room && departure) {
	 if(logs[i].status != ARRIVAL || logs[i].room != atol(room)) {
	    //if they left the last room already, can't leave again
	    //if the room they are trying to leave is not the room they are in
		fprintf(stdout, "invalid\n");
	    close(log_fd);
	    return EXIT;
	 }
	 //Case of -L (leaving gallery as a whole
      } else if (!room && departure) {
	 if(logs[i].status != LEAVING && logs[i].room != GALLERY) {
	    //to leave gallery, must leave previous room or just entered gallery
		fprintf(stdout, "invalid\n");
	    close(log_fd);
	    return EXIT;
	 }
      }
	 } else {
      if (!arrival || room || !timestamp || !(employee_name || guest_name) || !(token)) {
		fprintf(stdout, "invalid\n");
	 	return EXIT;
      }
	 }
      
   		////////////////
   		/* APPEND LOG */
   		////////////////
	  
      //add in new log
      logs[header_data.num_of_logs] = log_s;
      //update last timestamp
      header_data.timestamp = log_s.timestamp;
      header_data.num_of_logs += 1;
      total = 0;
      
      //encrypt everything 
      int buffer_size = sizeof(Log_Header_Data)+sizeof(Log)*(header_data.num_of_logs);
      unsigned char *buffer = malloc(buffer_size);
      if(!(data_length = crypt((unsigned char*) &header_data, sizeof(Log_Header_Data),
			       buffer, token_hash, header_keys.iv2, ENCRYPT))){
	 return EXIT;
      }
      total+=data_length;
      if(!(data_length = crypt((unsigned char*) logs,
			       sizeof(Log)*(header_data.num_of_logs),
			       buffer+data_length, token_hash,
			       header_keys.iv2, ENCRYPT))){
	 return EXIT;
      }
      total+=data_length;
      hmac(buffer, total, header_keys.hmac, token_hash);
      
      //Write everything
      lseek(log_fd, 0, SEEK_SET);
      write(log_fd, &header_keys, sizeof(Log_Header_Keys));
      write(log_fd, buffer, total);
      close(log_fd);
      return 0;
   } else {
   		//////////////
   		/* NEW FILE */
   		//////////////
      
      //File does not exist, Setting up and creating Log for the First time
      unsigned char buffer[1024];
      
      iv_gen(header_keys.iv); //hash 
      iv_gen(header_keys.iv2); //hmac

      //first log can only be used with -A and no -R
      if (!arrival || room || !timestamp || !(employee_name || guest_name) || !(token)) {
		fprintf(stdout, "invalid\n");
	 	return EXIT;
      }

      log_fd = open(log, O_WRONLY | O_CREAT, 0664);
      if (log_fd < 0){ /* Error */ 
		fprintf(stdout, "invalid\n");
		return EXIT;
	  }
      
	  //hash the new token for validation later
      unsigned char *mes = malloc(strlen(token) + IV_LENGTH);
      //IV is for salt
      memcpy(mes, header_keys.iv, IV_LENGTH);
      memcpy(mes+IV_LENGTH, token, strlen(token));
   
      hash(mes, strlen(token)+IV_LENGTH, token_hash);
      free(mes);

	  //hmac the new token for validation later
	  hmac(token_hash, KEY_LENGTH, 
			  header_keys.token_hmac, header_keys.iv);

      if (guest_name){
	 strncpy(log_s.name.n, guest_name, MAX_NAME_LENGTH);
	 log_s.name.n[MAX_NAME_LENGTH] = '\0';
	 log_s.name.type = GUEST;
      } else if (employee_name) {
	 strncpy(log_s.name.n, employee_name, MAX_NAME_LENGTH);
	 log_s.name.n[MAX_NAME_LENGTH] = '\0';
	 log_s.name.type = EMPLOYEE;
      } else {
	 fprintf(stdout, "invalid\n");	  
	 close(log_fd);
	 return EXIT;
      }
      
      //validate that these are propper longs
      log_s.timestamp = atol(timestamp);
      log_s.room = GALLERY;
      log_s.status = ARRIVAL;
      
      header_data.timestamp = log_s.timestamp;
      header_data.num_of_logs = 1;
      
      Log *logs = malloc(sizeof(Log));
      logs[0] = log_s;
      int total = 0;
      //Enc the data
      if(!(data_length = crypt((unsigned char*) &header_data, sizeof(Log_Header_Data),
			buffer, token_hash, header_keys.iv2, ENCRYPT))){
	 
	 return EXIT;
      }
      total+=data_length;
      
	  if(!(data_length = crypt((unsigned char*) logs, sizeof(Log),
			       buffer+data_length, token_hash, header_keys.iv2, ENCRYPT))){
	 return EXIT;
      }
	  total+=data_length;
      
	  hmac(buffer, total, header_keys.hmac, token_hash);

      
      //Write the encrypts
	  free(logs);
      write(log_fd, (unsigned char*) &header_keys, sizeof(Log_Header_Keys));
      write(log_fd, buffer, total);
      close(log_fd);
	  return 0;
   }
   return 0;
}
