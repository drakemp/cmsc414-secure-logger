#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<ctype.h>

#include"log.h"
#include"crypt.h"

#define TRUE 1
#define FALSE 0

#define S 1
#define R 2
#define T 4
#define I 8

#define EXIT_BAD_ARG 255

int main(int argc, char *argv[]){
   int   opt = -1;
   char	*token=NULL,
      *guest_name=NULL,
      *employee_name=NULL,
      *log=NULL;
   short state = 0;
   int i  = 0;
  
   //Handle the arguments in any order!
      //////////////
      /*CHECK ARGS*/
      //////////////
   while ((opt = getopt(argc, argv, "K:SRE:G:T")) != -1) {
      switch(opt) {
      case 'I':
	 fprintf(stdout, "unimplemented\n");
	 exit(EXIT_BAD_ARG);
	 break;
      case 'T':
	 fprintf(stdout, "unimplemented\n");
	 exit(EXIT_BAD_ARG);
	 break;
	 
      case 'K':
	 token = optarg;
	 for (i = 0; i < strlen(token); i++) {
	    if (!isalpha(token[i]) && !isdigit(token[i])) {
	 	   fprintf(stdout, "invalid\n");
	       exit(EXIT_BAD_ARG);
	    }
	 }
	 break;
	 
      case 'S':
	 if (!state)
	    state = S;
	 else{
	 	fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      case 'R':
	 if (!state)
	    state = R;
	 else{
	 	fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      case 'E':
	 if (!guest_name) {
	    employee_name = optarg;
	    for (i = 0; i < strlen(employee_name); i++) {
	       if (!isalpha(employee_name[i])) {
	 	  fprintf(stdout, "invalid\n");
		  exit(EXIT_BAD_ARG);
	       }
	    }
	 }else{
	 	fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      case 'G':
	 if (!employee_name) {
	    guest_name = optarg;
	    for (i = 0; i < strlen(guest_name); i++) {
	       if (!isalpha(guest_name[i])) {
	 	  fprintf(stdout, "invalid\n");
		  exit(EXIT_BAD_ARG);
	       }
	    }
	 } else{
	 	fprintf(stdout, "invalid\n");
	    exit(EXIT_BAD_ARG);
	 }
	 break;
	 
      default:
	fprintf(stdout, "invalid\n");
	exit(EXIT_BAD_ARG);	    
        break;
      }
   }

   if(optind < argc && optind+1 == argc) {
      log = argv[optind];
	  for (i=0; i < strlen(log); i++) {
	 	   if (!isalpha(log[i]) && log[i] != '_' && log[i] != '/' 
				   && log[i] != '.' && !isdigit(log[i])) {
			fprintf(stdout, "invalid\n");
	 	   	exit(EXIT_BAD_ARG);
	 	   }
	  }
   } else {
	  fprintf(stdout, "invalid\n");
      exit(EXIT);
   }

   if (!state || !token){
	  fprintf(stdout, "invalid\n");
      exit(EXIT);
   }
   if (state == R){
      if (!guest_name && !employee_name){
	  fprintf(stdout, "invalid\n");
	 exit(EXIT);
      }
   }
   
      //////////////
      /*MAIN LOGIC*/
      //////////////
   Log_Header_Keys header_keys;
   Log_Header_Data header_data;
   int data_length;
  
   /* check if out file exists */
   if(access(log, F_OK) !=  -1){
     
	   ///////////////
      /*FILE EXISTS*/
      ///////////////
      
	  //Check to see if token is correct
      unsigned char *encrypted_data;
      int file_size, bytes_read, log_fd;
     
      //open file and get size
      log_fd = open(log, O_RDWR, 0664);
      file_size = lseek(log_fd, 0, SEEK_END);
      lseek(log_fd, 0, SEEK_SET);
      
      //read in header
      bytes_read = read(log_fd, &header_keys, sizeof(Log_Header_Keys));
      if (bytes_read != sizeof(Log_Header_Keys)) {
	return EXIT;
      }
      //Read in the encrypted data
      int log_bytes = file_size-sizeof(Log_Header_Keys); //Get log size
      encrypted_data = malloc(log_bytes);
      bytes_read = read(log_fd, encrypted_data, log_bytes);
      if (bytes_read != log_bytes) {
	 return EXIT;
      }
      
      unsigned char *mes = malloc(strlen(token) + IV_LENGTH);
      unsigned char token_hash[KEY_LENGTH];
      memcpy(mes, header_keys.iv, IV_LENGTH);
      memcpy(mes+IV_LENGTH, token, strlen(token));
     
      hash(mes, strlen(token)+IV_LENGTH, token_hash);
      //verify
      int hash_verify = hmac_verification(token_hash, KEY_LENGTH,
			  header_keys.token_hmac, header_keys.iv);
      
      if (!hash_verify) {
	 fprintf(stdout, "integrity violation\n");
	 return EXIT;
      }

      int hmac_verify = hmac_verification(encrypted_data, bytes_read,
					  header_keys.hmac, token_hash);
      if (!hmac_verify) {
	 fprintf(stdout, "integrity violation\n");
	 return EXIT;
      }
      
      if((data_length = crypt(encrypted_data, sizeof(Log_Header_Data),
			      (unsigned char *)&header_data, token_hash, 
			      header_keys.iv2, DECRYPT))){
	 return EXIT;
      }
      
      Log *logs = malloc(sizeof(Log)*(header_data.num_of_logs));
      
      //Decrypting logs all at once, add a 32 byte offset for header because
      //it was being padded 
      if((data_length = crypt(encrypted_data+32,
			      sizeof(Log)*(header_data.num_of_logs),
			      (unsigned char *)logs,
			      token_hash, header_keys.iv2, DECRYPT))){
	 return EXIT;
      }
      
      ///////////////////
      /*PROCESSING HERE*/
      ///////////////////
      
      if (state == S) {
	 print_current_state(logs, header_data.num_of_logs);
      } else if (state == R){
	 print_room_list(logs, header_data.num_of_logs,
			 guest_name? guest_name: employee_name,
			 guest_name? GUEST: EMPLOYEE);
      }
      free(encrypted_data);
	  free(mes);
	  free(logs);
      return 0;
   }
   fprintf(stdout, "invalid\n");
   return EXIT;
}
