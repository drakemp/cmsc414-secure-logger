#ifndef CRYPT_H
#define CRYPT_H
#include<stdio.h>
#include<stdio.h> 
#include<stdlib.h> 
#include<string.h> 
#include<openssl/evp.h> 
#include<openssl/rand.h>

#define ENCRYPT 1
#define DECRYPT 0

#define KEY_LENGTH 32
#define IV_LENGTH 16
#define SALT_LENGTH 8

size_t crypt(const unsigned char *in, int in_length, unsigned char *out,
	     const unsigned char *key,
	     const unsigned char *iv, int do_crypt);

int hmac(const unsigned char *in, size_t in_length,
	 unsigned char *out,
	 const unsigned char *iv);

int hash(const unsigned char *message, int length,
	 unsigned char *out);
   
int iv_gen(unsigned char *iv);

int hmac_verification(const unsigned char *in, size_t in_length,
		      const unsigned char *hmac_ver,
		      const unsigned char *iv);

#endif



#ifndef TRUE_FALSE
#define TRUE_FALSE

#define TRUE 1
#define FALSE 0

#endif
