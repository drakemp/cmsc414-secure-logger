#include"crypt.h"

/* encryption with aes-256-ctr where the iv is 0 */ 
size_t crypt(const unsigned char *in, int in_length, unsigned char *out, 
	     const unsigned char *key,
	     const unsigned char *iv, int do_crypt){    

   EVP_CIPHER_CTX *ctx; 
   int outlen;
   size_t total=0; //out will get set by EVP

   ctx = EVP_CIPHER_CTX_new(); 
   EVP_CipherInit_ex(ctx, EVP_aes_256_cbc(), NULL, NULL, NULL,do_crypt); 
   OPENSSL_assert(EVP_CIPHER_CTX_key_length(ctx) == KEY_LENGTH); 
   OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == IV_LENGTH); 
   
   /* Now we can set key and IV */ 
   EVP_CipherInit_ex(ctx, NULL, NULL, key, iv, do_crypt); 
   
   if (!EVP_CipherUpdate(ctx, out, &outlen, in, in_length)) { 
      /* Error */ fprintf(stderr, "Failed here\n"); EVP_CIPHER_CTX_free(ctx); return 0;
   } 
   total += outlen; 

   //padding? 
   if (!EVP_CipherFinal_ex(ctx, out+outlen, &outlen)) { 
      /* Error */ EVP_CIPHER_CTX_free(ctx); return 0; 
   } 
   total += outlen; 
   
   EVP_CIPHER_CTX_free(ctx); 
   return total; //returns the total number of bytes written   
}

// Uses SHA256 returns hash in 'out' and returns length of the hash
// returns 0 on failure
int hash(const unsigned char *message, int length,
	 unsigned char *out){ 
   OpenSSL_add_all_digests();
   EVP_MD_CTX *mdctx; 
   const EVP_MD *md; 
   unsigned int md_len;

   md = EVP_get_digestbyname("sha256"); 	    
   mdctx = EVP_MD_CTX_create(); 
   EVP_DigestInit_ex(mdctx, md, NULL); 
   if (!EVP_DigestUpdate(mdctx, message, length)){
      /* Error */ EVP_MD_CTX_destroy(mdctx); return 0;
   } 
   if (!EVP_DigestFinal_ex(mdctx, out, &md_len)){
      /* Error */ EVP_MD_CTX_destroy(mdctx); return 0;
   }
   EVP_MD_CTX_destroy(mdctx); 
   
   return md_len; 
} 

int hmac(const unsigned char *in, size_t in_length,
	 unsigned char *out, const unsigned char *iv){

   unsigned char salt_A[SALT_LENGTH]={0}, salt_B[SALT_LENGTH]={0};
   unsigned char *temp = malloc(in_length + SALT_LENGTH); 
   int i;

   //set up salts
   for (i=0; i<IV_LENGTH/2; i++)
      salt_A[i]=iv[i];
   for (; i<IV_LENGTH; i++)
      salt_B[i-IV_LENGTH/2]=iv[i];
   
   // Set up for hashing
   memcpy(temp, salt_A, SALT_LENGTH);
   memcpy(temp+SALT_LENGTH, in, in_length);

   if(!hash(temp, in_length+SALT_LENGTH, out)){
      /* Error */ free(temp); return 0;
   }

   memcpy(temp, salt_B, SALT_LENGTH);
   memcpy(temp+SALT_LENGTH, out, KEY_LENGTH);

   if (!hash(temp, KEY_LENGTH+SALT_LENGTH, out)){
      /* Error */ free(temp); return 0;
   }
   
   free(temp);
   return 0;
}

int iv_gen(unsigned char *iv) {
	int success = 0;
	RAND_poll();
	while (success != 1) {
		success = RAND_bytes(iv, 16);
	}
	return 0;
}

int hmac_verification(const unsigned char *message, size_t length,
		      const unsigned char *hmac_ver,
		      const unsigned char *iv){
   unsigned char hmac_mes[KEY_LENGTH]={0};
   int i;
   //hash message, then compare it to hmac_ver
   hmac(message, length, hmac_mes, iv);
   for (i = 0; i < KEY_LENGTH; i++){
      if(hmac_mes[i] != hmac_ver[i]){return FALSE;}
   }
   return TRUE;
}

