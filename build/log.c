#include<stdio.h>
#include<stdlib.h>
#include"log.h"


int is_name_in_bag(Log *bag, int length, Name name){
   int i;
   for (i = 0; i < length; i++){
      if (!strcmp(bag[i].name.n, name.n) && bag[i].name.type == name.type){
	 return TRUE;
      }
   }
   return FALSE;
}


int compare_name(const void *s1, const void *s2) {
	Log *log1 = (Log *)s1;
	Log *log2 = (Log *)s2;
	return strcmp(log1->name.n, log2->name.n);
}

int compare_room(const void *s1, const void *s2) {
	Log *log1 = (Log *)s1;
	Log *log2 = (Log *)s2;
	if (log1->room == log2->room){
	   return compare_name(s1,s2);
	} else if ((long)log1->room < (long)log2-> room){
	   return -1;
	} else {
	   return 1;
	}
}

void print_current_state(Log *logs, int length){
   int i, j = 0, size_of_bag = 0, size_of_left = 0;
   Log *bag = malloc(sizeof(Log)*length);
   Log *left = malloc(sizeof(Log)*length);
   for (i = length-1; i >= 0; i--){

      //if the name is in left, ignore it,
      //if name is in the bag already, ignore it
      if (!is_name_in_bag(left, size_of_left, logs[i].name)
	  && !is_name_in_bag(bag, j, logs[i].name)){
	 //if they left add to left
	 if ((long)logs[i].room == LEFT_GALLERY) {
	    left[size_of_left++] = logs[i];
	 } else { //otherwise add them to bag
	    bag[j++] = logs[i];
	 }
      }
   }

   //Sort bags by name using qsort and overriden compare
   size_of_bag = j;
   qsort(bag, size_of_bag, sizeof(Log), compare_name);
   qsort(left, size_of_left, sizeof(Log), compare_name);

   //print employees, first must be by itself since no comma
   int first = TRUE;
   for (i=0; i<size_of_bag; i++) {
      if (bag[i].name.type == EMPLOYEE) {
	 if (first)
	    printf("%s", bag[i].name.n);
	 else 
	    printf(",%s", bag[i].name.n);
	 first = FALSE;
      }
   }
   printf("\n");

   //print guests
   first = TRUE;
   for(i = 0; i < size_of_bag; i++){
      if (bag[i].name.type == GUEST){
	 if (first)
	    printf("%s", bag[i].name.n);
	 else 
	    printf(",%s", bag[i].name.n);
	 first = FALSE;
      }
   }
   printf("\n");

   
   /////////////
   //Print Room-by-room information
   //on which guest/employee is in what room
   /////////////

   qsort(bag, size_of_bag, sizeof(Log), compare_room);
   qsort(left, size_of_left, sizeof(Log), compare_room);

   for (i = 0; i < size_of_bag; i++){
      if ((long)bag[i].room >= 0 && bag[i].status != LEAVING) {
	 printf("%lu: ", bag[i].room);
	 break;
      }
   }
   first = TRUE;
   for(; i < size_of_bag; i++){
      if (bag[i].status == LEAVING) continue; /*Dont print people who left the rooms?*/
      if (first)
	 printf("%s", bag[i].name.n);
      else 
	 printf(",%s", bag[i].name.n);
      first = FALSE;
      
      if (i+1 < size_of_bag && bag[i].room != bag[i+1].room){
	 printf("\n%lu: ", bag[i+1].room);
	 first = TRUE;
	 continue;
      }
   }
   free(bag);
}


void print_room_list(Log *logs, int length, char *name, int type){
   int i, first = TRUE;
   for (i=0; i<length; i++){
      if (!strcmp(name, logs[i].name.n)){
	 if (logs[i].name.type != type){
	    fprintf(stderr, "Invalid type\n");
	    exit(EXIT);
	 }
	 if ((long)logs[i].room == GALLERY || (long)logs[i].room == LEFT_GALLERY){
	    continue;
	 }
	 
	 if (logs[i].status == ARRIVAL){
	    if (first)
	       printf("%lu", logs[i].room);
	    else
	       printf(",%lu", logs[i].room);
	    first = FALSE;
	 }
      }
   }
}
